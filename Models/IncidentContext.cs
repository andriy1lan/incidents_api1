using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;

namespace incidents.Models
{
    public class IncidentContext: DbContext
    {
        public DbSet<Incident> Incidents {get; set;}
        public DbSet<Account> Accounts {get; set;}
        public DbSet<Contact> Contacts {get; set;}
        
        public IncidentContext(DbContextOptions<IncidentContext> options)
            : base(options)
        {
               Database.EnsureCreated();
        }  
        /* 
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=IncidentDB2;Trusted_Connection=True;");
        } */

          //Use this seed with applying migration
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

       modelBuilder.Entity<Incident>()
              .HasData(
       new Incident { Id = new Guid("513596c6-61bf-44b7-bf27-08d8c69b4144"), Description = "Info of Case1" },
       new Incident { Id = new Guid("513596c6-45bf-44b7-bf27-08d8c69b4146"), Description = "Info of Case2" },
       new Incident { Id = new Guid("513596c6-56af-44b7-bf27-08d8c69b4176"), Description = "Info of Case3" });

       modelBuilder.Entity<Account>()
              .HasData(
       new Account { Name = "Andrr11", IncidentId = new Guid("513596c6-45bf-44b7-bf27-08d8c69b4146") },
       new Account { Name = "Roman11", IncidentId = new Guid("513596c6-45bf-44b7-bf27-08d8c69b4146") },
       new Account { Name = "Iryna11", IncidentId = new Guid("513596c6-56af-44b7-bf27-08d8c69b4176") },
       new Account { Name = "Olga11", IncidentId = new Guid("513596c6-61bf-44b7-bf27-08d8c69b4144")});
       
       modelBuilder.Entity<Contact>()
              .HasData(
       new Contact { FirstName = "Andriy", LastName = "Surname1", Email = "andriy11@i.ua", AccountName="Andrr11"},
       new Contact { FirstName = "Roman", LastName = "Surname2", Email = "roman11@i.ua", AccountName="Roman11"},
       new Contact { FirstName = "Iryna", LastName = "Surname3", Email = "iryna11@i.ua", AccountName="Iryna11"},
       new Contact { FirstName = "Olga", LastName = "Surname4", Email = "olga11@i.ua", AccountName="Olga11"},
       new Contact { FirstName = "Olga", LastName = "Surname5", Email = "olga12@i.ua", AccountName="Olga11"});
       }
       
    }
}