using Newtonsoft.Json;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
namespace incidents.Models
{
    public class Account
    {
        [Key]
        public string Name {get;set;}
        public Guid? IncidentId {get; set;}
        [JsonIgnore]
        public virtual ICollection<Contact> Contacts  {get; set;}
    }
}