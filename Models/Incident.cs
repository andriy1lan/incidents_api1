using System;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace incidents.Models
{
    public class Incident
    {
       [Key]
       [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
       public Guid Id { get; set; }
       public string Description { get; set; }
       [JsonIgnore]
       public virtual ICollection<Account> Accounts  {get; set;}
    }
}