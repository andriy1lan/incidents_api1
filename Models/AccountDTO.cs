using System;
namespace incidents.Models
{
    public class AccountDTO
    {
        public string Name {get;set;}
        public Guid? IncidentId {get; set;}
        public string ContactEmail {get;set;}
    }
}