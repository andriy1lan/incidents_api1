using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace incidents.Models
{
    public class Contact
    {
        public string FirstName { get; set;}
        public string LastName { get; set;}
        [Key]
        public string Email { get; set;}
        public string AccountName { get; set;}
    }
}