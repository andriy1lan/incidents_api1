using System.Security.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using incidents.Models;
using System.Collections.ObjectModel;

namespace incidents.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IncidentsController : ControllerBase
    {
        
        private IncidentContext context;
        public IncidentsController (IncidentContext context) {this.context=context;}
        //private IncidentContext context=new IncidentContext();

        // GET api/incidents
        [HttpGet]
        public ActionResult<IList<Incident>> GetIncidents()
        {
            var incidents = context.Incidents.ToList();
            if (incidents.Count!= 0)
            {
                return Ok(incidents);
            }
            else { return NotFound(); }
        }

        // GET api/incidents/5
        [HttpGet("{id}")]
        public ActionResult<Incident> GetIncident(Guid id)
        {
            var incident = context.Incidents.Where(i=>i.Id.Equals(id)).FirstOrDefault();
            if (incident != null)
            {
                return Ok(incident);
            }
            else { return NotFound(); }
        }

        [HttpPost]
        public ActionResult<Incident> PostIncident([FromBody]IncidentDTO incidentDTO)
        {
            var account=context.Accounts.Where(a=>a.Name.Equals(incidentDTO.AccountName)).FirstOrDefault();
            if (account==null) return NotFound();
            if (String.IsNullOrEmpty(incidentDTO.Email)) return BadRequest("No email. Provide an email");
            var contact=context.Contacts.Where(a=>a.Email.Equals(incidentDTO.Email)).FirstOrDefault();
            if (contact==null) {
                var newContact=new Contact {
                FirstName=incidentDTO.FirstName, LastName=incidentDTO.LastName,
                Email=incidentDTO.Email, AccountName=incidentDTO.AccountName
                };
                if (account.Contacts==null)
                {
                    account.Contacts=new Collection<Contact>();
                }
                account.Contacts.Add(newContact); //Link Contact to Account
            }
            else contact.AccountName=incidentDTO.AccountName; //Link Contact to Account 
            var incident=new Incident {Description=incidentDTO.Description};
            context.Incidents.Add(incident);
            account.IncidentId=incident.Id; //Link Account to Incident
            context.SaveChanges();
            return CreatedAtAction(nameof(GetIncident), new { id = incident.Id }, incident);
        }

    }
}