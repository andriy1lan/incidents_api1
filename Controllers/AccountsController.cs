using System.Collections.ObjectModel;
using System.Data;
using System.Reflection.Metadata;
using System.ComponentModel;
using System.Security.AccessControl;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.ComponentModel.Design.Serialization;
using System.Diagnostics.Contracts;
using System.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using incidents.Models;

namespace incidents.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
         private IncidentContext context;
         public AccountsController (IncidentContext context) {this.context=context;}
         //private IncidentContext context=new IncidentContext();

        // GET api/accounts
        [HttpGet]
        public ActionResult<IList<Account>> GetAccounts()
        {
            var accounts = context.Accounts.ToList();
            if (accounts.Count!= 0)
            {
                return Ok(accounts);
            }
            else { return NotFound(); }
        }

        // GET api/accounts/5
        [HttpGet("{name}")]
        public ActionResult<Account> GetAccount(string name)
        {
            var account = context.Accounts.Where(a=>a.Name.Equals(name)).FirstOrDefault();
            if (account!= null)
            {
                var incident=context.Incidents.Where(a=>a.Id.Equals(account.IncidentId)).FirstOrDefault();
                return Ok(account);
            }
            else { return NotFound(); }
        }

        [HttpPost]
        public ActionResult<Account> PostAccount([FromBody]AccountDTO accountDTO)
        {   
            if (String.IsNullOrEmpty(accountDTO.Name)) return BadRequest("No Account Name. Enter account name");
            var contact=context.Contacts.Where(a=>a.Email.Equals(accountDTO.ContactEmail)).FirstOrDefault();
            if (contact==null) return BadRequest("Cannot create Account without valid Contact Email");
            Account newAccount=new Account {
                Name=accountDTO.Name,
                IncidentId=accountDTO.IncidentId
            };
            newAccount.Contacts=new Collection<Contact>();
            newAccount.Contacts.Add(contact); //Link Contact to Account
            context.Accounts.Add(newAccount);
            try{
            context.SaveChanges(); }
            catch (Exception ex) { string message=null;
               if (ex.InnerException!= null) message=ex.InnerException.Message;
               else message=ex.Message;
               return StatusCode(500,message);
            }
            return StatusCode(201,newAccount);
        }
    }
}