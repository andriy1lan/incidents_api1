using System.Runtime.CompilerServices;
using System.ComponentModel.Design.Serialization;
using System.Diagnostics.Contracts;
using System.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using incidents.Models;
using System.Collections.ObjectModel;
using Microsoft.EntityFrameworkCore;

namespace incidents.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactsController : ControllerBase
    {
        
        private IncidentContext context;
        public ContactsController (IncidentContext context) {this.context=context;}
        //private IncidentContext context=new IncidentContext();

        // GET api/contacts
        [HttpGet]
        public ActionResult<IList<Contact>> GetContacts()
        {
            var contacts = context.Contacts.ToList();
            if (contacts.Count!= 0)
            {
                return Ok(contacts);
            }
            else { return NotFound(); }
        }

        // GET api/contacts/5
        [HttpGet("{email}")]
        public ActionResult<Contact> GetContact(string email)
        {
            var contact = context.Contacts.Where(a=>a.Email.Equals(email)).FirstOrDefault();
            if (contact!= null)
            {
                return Ok(contact);
            }
            else { return NotFound(); }
        }

        [HttpPost]
        public ActionResult<Contact> PostContact([FromBody]Contact contact)
        {
            /* Method to add contact with parent account name guid generation
            var account=context.Accounts.Where(a=>a.Name.Equals(contact.AccountName)).FirstOrDefault();
            if (account==null) {
                account=new Account();
                account.Name=contact.AccountName;
                context.Accounts.Add(account);
            }
            if (account.Contacts==null) account.Contacts=new Collection<Contact>();
            account.Contacts.Add(contact);
            */
            if (String.IsNullOrEmpty(contact.Email)) return BadRequest("No email. Enter correct email");
            context.Contacts.Add(contact);
            try{
            context.SaveChanges(); }
            catch (Exception ex) { string message=null;
               if (ex.InnerException!= null) message=ex.InnerException.Message;
               else message=ex.Message;
               return StatusCode(500,message);
            }
            return StatusCode(201,contact);
        }
    }
}