using incidents.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace incidents.Data
{
    public static class DataSeeder
    {
        public static void Seed(IncidentContext context)
    {
       if (!context.Incidents.Any())
        {
       var incidents = new List<Incident>
            {
       new Incident { Id = new Guid("513596c7-61bf-44b7-bf27-08d8c69b4144"), Description = "Info of Case1" },
       new Incident { Id = new Guid("513596c8-45bf-44b7-bf27-08d8c69b4146"), Description = "Info of Case2" },
       new Incident { Id = new Guid("513596c9-56af-44b7-bf27-08d8c69b4176"), Description = "Info of Case3" }
            };
       context.AddRange(incidents);
        }
        
       if (!context.Accounts.Any())
        {
       var accounts = new List<Account>
            {
       new Account { Name = "Andrr11", IncidentId = new Guid("513596c8-45bf-44b7-bf27-08d8c69b4146") },
       new Account { Name = "Roman11", IncidentId = new Guid("513596c8-45bf-44b7-bf27-08d8c69b4146") },
       new Account { Name = "Iryna12", IncidentId = new Guid("513596c6-56af-44b7-bf27-08d8c69b4176") },
       new Account { Name = "Olga11", IncidentId = new Guid("513596c7-61bf-44b7-bf27-08d8c69b4144")}
            };
        context.AddRange(accounts);
        }

      if (!context.Contacts.Any())
        {
        var contacts = new List<Contact>
            {
       new Contact { FirstName = "Andriy", LastName = "Surname1", Email = "andriy11@i.ua", AccountName="Andrr11"},
       new Contact { FirstName = "Roman", LastName = "Surname2", Email = "roman11@i.ua", AccountName="Roman11"},
       new Contact { FirstName = "Iryna", LastName = "Surname3", Email = "iryna12@i.ua", AccountName="Iryna11"},
       new Contact { FirstName = "Olga", LastName = "Surname4", Email = "olga11@i.ua", AccountName="Olga11"},
       new Contact { FirstName = "Olga", LastName = "Surname5", Email = "olga12@i.ua", AccountName="Olga11"}
            };
         context.AddRange(contacts);     
        }  

       context.SaveChanges();
    }
    }
}